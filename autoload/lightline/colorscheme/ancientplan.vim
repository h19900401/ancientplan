
" ============================
" COLOR PALETTE {{{1
" ============================

let s:blue      = '#3465a4'
" let s:lightblue = '#81A3CF'
let s:black     = '#000000'
let s:gray      = '#333333'
let s:lightgray = '#53586F'
let s:white     = '#d3d7cf'
let s:yellow    = '#c4a000'
let s:red       = '#cc0000'

" " Default gui colors if background is *light* and no custom palette is used.
" " This is the Tango theme from gnome-terminal.
" let s:default_light = {
"   \'text':       '#000000',
"   \'background': '#ffffff',
"   \'black':      '#000000',  'dark_grey':      '#555753',
"   \'red':        '#cc0000',  'bright_red':     '#ef2929',
"   \'green':      '#4e9a06',  'bright_green':   '#8ae234',
"   \'yellow':     '#c4a000',  'bright_yellow':  '#fce94f',
"   \'blue':       '#3465a4',  'bright_blue':    '#729fcf',
"   \'magenta':    '#75507b',  'bright_magenta': '#ad7fa8',
"   \'cyan':       '#06989a',  'bright_cyan':    '#34e2e2',
"   \'white':      '#d3d7cf',  'bright_white':   '#eeeeec',
"   \}

" " Default gui colors if background is *dark* and no custom palette is used.
" " This is the Tango theme from gnome-terminal.
" let s:default_dark = {
"   \'text':       '#d3d7cf',
"   \'background': '#222222',
"   \'black':      '#000000',  'dark_grey':      '#555753',
"   \'red':        '#cc0000',  'bright_red':     '#ef2929',
"   \'green':      '#4e9a06',  'bright_green':   '#8ae234',
"   \'yellow':     '#c4a000',  'bright_yellow':  '#fce94f',
"   \'blue':       '#3465a4',  'bright_blue':    '#729fcf',
"   \'magenta':    '#75507b',  'bright_magenta': '#ad7fa8',
"   \'cyan':       '#06989a',  'bright_cyan':    '#34e2e2',
"   \'white':      '#d3d7cf',  'bright_white':   '#eeeeec',
"   \}


" " choose default colors based on background
" if &background == 'light'
"   let s:palette = s:default_light
" else
"   let s:palette = s:default_dark
" endif

" ============================
" THEMING {{{1
" ============================

" ----------------------------
" Modes {{{2
" ----------------------------

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

" ----------------------------
" Normal {{{2
" ----------------------------

let s:p.normal.left     = [[s:white, s:blue], [s:black, s:white]]
let s:p.normal.middle   = [[s:black, s:white]]
let s:p.normal.right    = [[s:white, s:blue], [s:black, s:white]]
let s:p.normal.error    = [[s:red, s:white]]
let s:p.normal.warning  = [[s:yellow, s:white]]

"" ----------------------------
"" Inactive {{{2
"" ----------------------------

let s:p.inactive.left   = [[s:blue, s:white], [s:white, s:blue]]
let s:p.inactive.middle = [[s:white, s:black]]
let s:p.inactive.right  = [[s:white, s:blue], [s:blue, s:white]]

"" ----------------------------
"" Insert/Replace/Visual {{{2
"" ----------------------------

let s:p.insert.left     = [[s:black, s:white], [s:white, s:blue]]
let s:p.replace.left    = [[s:black, s:white], [s:white, s:blue]]
let s:p.visual.left     = [[s:black, s:white], [s:white, s:blue]]

"" ----------------------------
"" Tab line {{{2
"" ----------------------------

let s:p.tabline.tabsel    = [[s:white, s:blue]]
let s:p.tabline.left      = [[s:black, s:white]]
let s:p.tabline.middle  = [[s:blue, s:white]]
let s:p.tabline.right   = copy(s:p.normal.right)

" ----------------------------
" Autoload {{{2
" ----------------------------

let g:lightline#colorscheme#ancientplan#palette = lightline#colorscheme#fill(s:p)
